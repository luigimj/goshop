<?php
require_once 'vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
$request = Request::createFromGlobals();
$uri = $request->getPathInfo();
$baseUrl = 'http://localhost:5000/';

$loader = new Twig_Loader_Filesystem(array('./views'));
$twig = new Twig_Environment($loader);

switch ($uri) {
  // website
  case '/':
    echo $twig->render('default/index.html.twig');
    break;
  case '/register-customer':
    echo $twig->render('default/registerCustomer.html.twig');
    break;
  case '/shopping-cart':
    echo $twig->render('default/shoppingCart.html.twig');
    break;
  case '/categories':
    echo $twig->render('default/categories.html.twig');
    break;
  case '/products':
    echo $twig->render('default/products.html.twig');
    break;
  case '/product-details':
    echo $twig->render('default/productDetails.html.twig');
    break;
  case '/profile':
    echo $twig->render('default/profile.html.twig');
    break;
   case '/orders':
    echo $twig->render('default/orders.html.twig');
    break;
  case '/wish-list':
    echo $twig->render('default/wishList.html.twig');
    break;
  case '/payment':
    echo $twig->render('default/payment.html.twig');
    break;
  default:
    $twig->render('default/index.html.twig');
    break;
}
