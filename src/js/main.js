console.log('JS File load success');

// Show-hide login panel
$('.showLogin').on('click', function () {
  $('.header_login').slideToggle();
  //$('#modalLogin').modal('show');
});

// Show-hide password
$('.showPassword').on('click', function() {
    $(this).toggleClass("dripicons-eye dripicons-eye-slash");
    var input = $($(this).attr('data-toggle'));
    if (input.attr('type') == 'password') {
      input.attr('type', 'text');
    } else {
      input.attr('type', 'password');
    }
  });

// Hide cookies banner
$('.hideElement').on('click', function () {
  $('.cookie-banner').fadeOut();
});

$('#searchInput').on('focus', function () {
  $('.results').show();
});

$('#searchInput').on('blur', function () {
  $('.results').hide();
});

/********* VALIDATIONS *********/

// Configure jQuery Validation
$.validator.setDefaults({
  errorElement: "div",
  errorClass: 'invalid-feedback',
  highlight: function (element) {
    $(element).addClass('is-invalid').removeClass('is-valid');
  },
  unhighlight: function (element) {
    $(element).removeClass('is-invalid').addClass('is-valid');
  }
});

$('#loginForm').validate({
  submitHandler: function() { alert("Submitted!") }
});

$('#registerCustomer-form').validate({
  submitHandler: function() { alert("Submitted!") }
});


// Wizard

function paintProgressBar(progressBarObject, direction) {
  var numberOfSteps = $(progressBarObject).attr('data-number-of-steps');
	var currentValue = $(progressBarObject).attr('data-current-value');
  var newValue = 0;

  if(direction == 'right') {
    newValue = Number(currentValue) + ( 100 / numberOfSteps );
	}
	else if(direction == 'left') {
		newValue = Number(currentValue) - ( 100 / numberOfSteps );
  }

  $(progressBarObject).attr({
    'style': 'width: ' + newValue + '%;',
    'data-current-value': newValue
  });
}

// Do all wizard-step not clickable
$(".wizard-step").on('click', function () {
  return false;
});

// Next step
$('.wizard .btn-next').on( 'click', function() {
  //var parentTabPane = $(this).parents('.d-flex');

  // navigation steps / progress steps
  var currentActiveStep = $(this).parents('.wizard').find('.wizard-step.active');
  var progressBarObject = $(this).parents('.wizard').find('.wizard-progress-line');
  var nextStep = true;

  if (nextStep) {
    $(currentActiveStep).removeClass('active').addClass('activated').parent().next().children().removeClass('disabled').tab('show');
    paintProgressBar(progressBarObject, 'right');
  }
});

// Previous step
$('.wizard .btn-previous').on('click', function () {
  // navigation steps / progress steps
  var currentActiveStep = $(this).parents('.wizard').find('.wizard-step.active');
  var progressBarObject = $(this).parents('.wizard').find('.wizard-progress-line');

  $(currentActiveStep).removeClass('active').addClass('disabled').parent().prev().children().tab('show');
  paintProgressBar(progressBarObject, 'left');
});
